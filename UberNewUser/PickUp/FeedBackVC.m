//
//  FeedBackVC.m
//  UberNewUser
//
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"

@interface FeedBackVC ()
{
    NSMutableDictionary *dictParamPayPal;
    NSString *strRequest_id,*strId,*strToken;
    //UIButton *button;
    BOOL paid;
}

@end

@implementation FeedBackVC

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    dictParamPayPal=[[NSMutableDictionary alloc] init];
    strId=[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID];
    strToken=[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN];
    strRequest_id=[[NSUserDefaults standardUserDefaults] objectForKey:PREF_REQ_ID];
    
    //status = PAYMENTSTATUS_CANCELED;
    
    [self SetLocalization];
    self.navigationController.navigationBarHidden=NO;
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    self.lblFirstName.text=[arrName objectAtIndex:0];
    self.lblLastName.text=[arrName objectAtIndex:1];
    self.lblDistance.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"distance"] floatValue],[dictBillInfo valueForKey:@"unit"]];
    self.lblTIme.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"time" ] floatValue],NSLocalizedString(@"Mins", nil)];
    
    [self.imgUser applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    self.viewForBill.hidden=NO;
    [self.btnConfirm setHidden:NO];
    //[button setHidden:NO];
    self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    
    /*button = [[PayPal getPayPalInst]
     getPayButtonWithTarget:self
     andAction:@selector(checkPayment)
     andButtonType:BUTTON_294x43
     andButtonText:BUTTON_TEXT_PAY];*/
    
    //[button setFrame:self.btnConfirm.frame];
    //[button setFrame:CGRectMake((self.view.frame.size.width-294)/2, self.view.frame.size.height-63, 294, 43)];
    
    //[self.view addSubview:button];
    
    [self.btnFeedBack setTitle:@"  Invoice" forState:UIControlStateNormal];
    
    if([[dictBillInfo valueForKey:@"is_bank_detail_added"] intValue]==0)
    {
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"103", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"MAKE_PAYMENT", nil),nil];
        alert.tag=3000;
        [alert show];
    }
    else
    {
        if([[dictBillInfo valueForKey:@"is_paid"] intValue]==0 && [dictBillInfo valueForKey:@"total"]>0)
        {
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"PAYMENT_FAILED", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"MAKE_PAYMENT", nil),NSLocalizedString(@"ADD_NEW_CARD", nil), nil];
            alert.tag=2000;
            [alert show];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setPriceValue];
    [self customSetup];
}
- (void)viewDidAppear:(BOOL)animated
{
    // [self customSetup];
    [[AppDelegate sharedAppDelegate] hideLoadingView];
}

-(void)checkPaymentService
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[PREF objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:[PREF objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CHECK_PAYMENT withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Payment status--->%@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [APPDELEGATE showToastMessage:@"Your payment has been succesfully done"];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     
                     if([str isEqualToString:@"103"])
                     {
                         UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(str, nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"MAKE_PAYMENT", nil),nil];
                         alert.tag=3000;
                         [alert show];
                     }
                     else
                     {
                         UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"PAYMENT_FAILED", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"MAKE_PAYMENT", nil),NSLocalizedString(@"ADD_NEW_CARD", nil), nil];
                         alert.tag=2000;
                         [alert show];
                     }
                 }
             }
         }];
    }
    
}

-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lBasePrice.text=NSLocalizedString(@"BASE PRICE", nil);
    self.lDistanceCost.text=NSLocalizedString(@"DISTANCE COST", nil);
    self.lTimeCost.text=NSLocalizedString(@"TIME COST", nil);
    self.lPromoBonus.text=NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lreferalBonus.text=NSLocalizedString(@"REFERRAL BOUNCE", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    self.lComment.text=NSLocalizedString(@"COMMENT1", nil);
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnFeedBack addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==2000)
    {
        NSLog(@"index:%ld",(long)buttonIndex);
        
        if(buttonIndex == 0)
        {
            [self checkPaymentService];
        }
        else
        {
            UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
            
            PickUpVC *ViewObj;
            
            ViewObj=(PickUpVC *)[nav.childViewControllers objectAtIndex:0];
            if(ViewObj!=nil)
                [ViewObj goToSetting:@"segueToPayment"];
        }
    }
    if(alertView.tag == 3000)
    {
        if(buttonIndex==0)
        {
            [self checkPaymentService];
        }
    }
}

#pragma mark-
#pragma mark- Set Invoice Details

-(void)setPriceValue
{
    self.lblBasePrice.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"base_price"] floatValue]];
    self.lblDistCost.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"distance_cost"] floatValue]];
    self.lblTimeCost.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"time_cost"] floatValue]];
    self.lblTotal.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"total"] floatValue]];
    self.lblRferralBouns.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"referral_bonus"] floatValue]];
    self.lblPromoBouns.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"promo_bonus"] floatValue]];
    self.lblDistance.text=[NSString stringWithFormat:@"%.2f %@",[[dictBillInfo valueForKey:@"distance"] floatValue],[dictBillInfo valueForKey:@"unit"]];
    float totalDist=[[dictBillInfo valueForKey:@"distance_cost"] floatValue];
    float Dist=[[dictBillInfo valueForKey:@"distance"]floatValue];
    
    /*if([[dictBillInfo valueForKey:@"payment_type"]isEqualToString:@"1"])
     {
     self.lblPaymentMode.text = @"Cash";
     }
     else
     {
     self.lblPaymentMode.text = @"Card";
     }*/
    
    self.lblPaymentMode.text = @"Card";
    if ([[dictBillInfo valueForKey:@"unit"]isEqualToString:@"kms"])
    {
        totalDist=totalDist*0.621317;
        Dist=Dist*0.621371;
    }
    if(Dist!=0)
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"$%.2f %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
    }
    else
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"$0 %@",NSLocalizedString(@"per mile", nil)];
    }
    
    float totalTime=[[dictBillInfo valueForKey:@"time_cost"] floatValue];
    float Time=[[dictBillInfo valueForKey:@"time"]floatValue];
    if(Time!=0)
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"$%.2f %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
    }
    else
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"$0 %@",NSLocalizedString(@"per mins", nil)];
    }
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.lblDistance.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblDistCost.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblBasePrice.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblDistance.font=[UberStyleGuide fontRegular:15.0f];
    self.lblPerDist.font=[UberStyleGuide fontRegularLight:10.30f];
    self.lblPerTime.font=[UberStyleGuide fontRegularLight:10.30f];
    self.lblTIme.font=[UberStyleGuide fontRegular:15.0f];
    self.lblTimeCost.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblTotal.font=[UberStyleGuide fontRegular:42.0f];
    self.lblFirstName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.lblLastName.font=[UberStyleGuide fontRegularLight:20.0f];
    // self.btnFeedBack.titleLabel.font=[UberStyleGuide fontRegularLight:20.0f];
    self.btnFeedBack=[APPDELEGATE setBoldFontDiscriptor:self.btnFeedBack];
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
    
    self.lblPromoBouns.font=[UberStyleGuide fontRegularLight:20.70f];
    self.lblRferralBouns.font=[UberStyleGuide fontRegularLight:20.70f];
    self.btnConfirm.titleLabel.font=[UberStyleGuide fontRegularBold];
    self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];
    
    self.lBasePrice.font = [UberStyleGuide fontRegularLight];
    self.lDistanceCost.font = [UberStyleGuide fontRegularLight];
    self.lTimeCost.font = [UberStyleGuide fontRegularLight];
    self.lreferalBonus.font = [UberStyleGuide fontRegularLight];
    self.lPromoBonus.font = [UberStyleGuide fontRegularLight];
}

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)onClickBackBtn:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)submitBtnPressed:(id)sender
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        
        [self.txtComments resignFirstResponder];
        RBRatings rating=[ratingView getcurrentRatings];
        float rate=rating/2.0;
        if (rating%2 != 0)
        {
            rate += 0.5;
        }
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        else
        {
            NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
            NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[PREF objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:strForUserId forKey:PARAM_ID];
            [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
            [dictParam setObject:[NSString stringWithFormat:@"%f",rate] forKey:PARAM_RATING];
            NSString *commt=self.txtComments.text;
            if([commt isEqualToString:NSLocalizedString(@"COMMENT", nil)])
            {
                [dictParam setObject:@"" forKey:PARAM_COMMENT];
            }
            else
            {
                [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];
            }
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"%@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                         [PREF removeObjectForKey:PREF_REQ_ID];
                         [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"PaymentWithPaypal"];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }
                 
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
             }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

/*#pragma mark - Paypal Integration
 
 - (void)parallelPayment
 {
 if(paid || [[NSUserDefaults standardUserDefaults]boolForKey:@"PaymentWithPaypal"]==YES)
 {
 [button setHidden:YES];
 [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
 self.viewForBill.hidden=YES;
 self.navigationController.navigationBarHidden=NO;
 
 ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(120, 20) AndPosition:CGPointMake(100, 195)];
 ratingView.backgroundColor=[UIColor clearColor];
 [self.view addSubview:ratingView];
 }
 else
 {
 //NSDictionary*dictBill=[self.dictR valueForKey:@"bill"];
 
 //NSDictionary *dictAdmin=[dictBill valueForKey:@"admin"];
 //NSDictionary *dictWalker=[dictBill valueForKey:@"walker"];
 
 //dismiss any native keyboards
 //[preapprovalField resignFirstResponder];
 
 //optional, set shippingEnabled to TRUE if you want to display shipping
 //options to the user, default: TRUE
 [PayPal getPayPalInst];
 [PayPal getPayPalInst].shippingEnabled = TRUE;
 
 //    [PayPal getPayPalInst].feePayer;
 
 //optional, set dynamicAmountUpdateEnabled to TRUE if you want to compute
 //shipping and tax based on the user's address choice, default: FALSE
 [PayPal getPayPalInst].dynamicAmountUpdateEnabled = FALSE;
 
 
 //optional, choose who pays the fee, default: FEEPAYER_EACHRECEIVER
 [PayPal getPayPalInst].feePayer = FEEPAYER_PRIMARYRECEIVER;
 
 //for a payment with multiple recipients, use a PayPalAdvancedPayment object
 PayPalAdvancedPayment *payment = [[PayPalAdvancedPayment alloc] init];
 
 //NSString *curency = [[NSUserDefaults standardUserDefaults]valueForKey:@"currency"];
 
 //NSString *currency = @"€";
 
 //payment.paymentCurrency = [NSString stringWithFormat:@"%@",[currency isEqualToString:@"$"]?@"USD":@"EUR"];
 
 payment.paymentCurrency = @"USD";
 
 // A payment note applied to all recipients.
 payment.memo = @"A Note applied to all recipients";
 
 //receiverPaymentDetails is a list of PPReceiverPaymentDetails objects
 payment.receiverPaymentDetails = [NSMutableArray array];
 
 //Frank's Robert's Julie's Bear Parts;
 //NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
 
 NSString *nameUser = @"Kartrip";
 NSString *mailUser = [dictBillInfo valueForKey:@"walker_paypal_email"];
 //NSString *amountUser =[dictBillInfo valueForKey:@"walker_total"];
 NSString *amountUser =[NSString stringWithFormat:@"%.2f",[[dictBillInfo valueForKey:@"walker_total"] floatValue]];
 
 NSString *nameAdmin = @"Admin";
 NSString *mailAdmin = [dictBillInfo valueForKey:@"admin_paypal_email"];
 //NSString *amountAdmin =[dictBillInfo valueForKey:@"admin_total"];
 NSString *amountAdmin =[NSString stringWithFormat:@"%.2f",[[dictBillInfo valueForKey:@"admin_total"] floatValue]];
 
 // NSLog(@"dict admin : %@ and user :%@",dictAdmin,dictWalker);
 
 PayPalReceiverPaymentDetails *details = [[PayPalReceiverPaymentDetails alloc] init];
 
 float totalAmount;
 totalAmount=[amountUser floatValue]+[amountAdmin floatValue];
 
 details.isPrimary=YES;
 details.recipient = mailUser;
 details.merchantName = nameUser;
 details.subTotal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",totalAmount]];
 
 details.invoiceData.totalTax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",0.00]];
 
 NSLog(@"user payment :%@",details.subTotal);
 
 details.invoiceData = [[PayPalInvoiceData alloc] init];
 
 details.invoiceData.invoiceItems = [NSMutableArray array];
 PayPalInvoiceItem *item = [[PayPalInvoiceItem alloc] init];
 item.totalPrice = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",[details.subTotal floatValue]]];
 
 NSMutableArray *arrItem = [[NSMutableArray alloc]init];
 
 arrItem = [[dictBillInfo valueForKey:@"type"] valueForKey:@"name"];
 
 item.name = [arrItem objectAtIndex:0]; //[nameArray objectAtIndex:i];
 [details.invoiceData.invoiceItems addObject:item];
 
 // paypal split by admin :
 
 PayPalReceiverPaymentDetails *detailsAdmin = [[PayPalReceiverPaymentDetails alloc] init];
 
 detailsAdmin.isPrimary=NO;
 
 detailsAdmin.recipient = mailAdmin;
 detailsAdmin.merchantName = nameAdmin;
 detailsAdmin.subTotal = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",[amountAdmin floatValue]]];
 
 NSLog(@"admin payment :%@",detailsAdmin.subTotal);
 
 // total taxi
 detailsAdmin.invoiceData.totalTax = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",0.00]];
 
 detailsAdmin.invoiceData=[[PayPalInvoiceData alloc] init];
 detailsAdmin.invoiceData.invoiceItems = [NSMutableArray array];
 PayPalInvoiceItem *itemAdmin = [[PayPalInvoiceItem alloc] init];
 itemAdmin.totalPrice =[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f",[detailsAdmin.subTotal floatValue]]];
 
 //detailsAdmin.subTotal;
 itemAdmin.name = nameAdmin;
 [detailsAdmin.invoiceData.invoiceItems addObject:itemAdmin];
 NSLog(@"detail :%@",details);
 
 [payment.receiverPaymentDetails addObject:details];
 [payment.receiverPaymentDetails addObject:detailsAdmin];
 [[PayPal getPayPalInst] advancedCheckoutWithPayment:payment];
 }
 }
 
 -(void)sendPaymentTransactionID:(NSString *)transactionID
 {
 NSLog(@"transactionID :%@",transactionID);
 
 if(![transactionID isEqualToString:@""])
 {
 [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"PaymentWithPaypal"];
 if([APPDELEGATE connected])
 {
 [APPDELEGATE showLoadingWithTitle:@"Please wait...."];
 
 NSMutableDictionary *dictParams = [[NSMutableDictionary alloc]init];
 [dictParams setValue:strId forKey:PARAM_ID];
 [dictParams setValue:strToken forKey:PARAM_TOKEN];
 [dictParams setValue:strRequest_id forKey:PARAM_REQUEST_ID];
 [dictParams setValue:transactionID forKey:@"paypal_id"];
 
 NSLog(@"dictonary parameter for payment by payal :%@",dictParams);
 
 AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
 [afn getDataFromPath:FILE_PAYPAL_PAYMENT withParamData:dictParams withBlock:^(id response, NSError *error)
 {
 if (response)
 {
 if([[response valueForKey:@"success"] intValue]==1)
 {
 // confirm button pressed code
 
 [button setHidden:YES];
 [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
 self.viewForBill.hidden=YES;
 self.navigationController.navigationBarHidden=NO;
 
 ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(120, 20) AndPosition:CGPointMake(100, 195)];
 ratingView.backgroundColor=[UIColor clearColor];
 [self.view addSubview:ratingView];
 
 NSLog(@"Payal API Response - %@",response);
 [APPDELEGATE showToastMessage:@"Congratulations, your payment is successfully done!"];
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 }
 
 }
 else if (error)
 {
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Internal Server Error"
 message:@"Your Payment has been recieved by PayPal. Once the services has been restored the same will be reflected in History"
 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 [alertView show];
 }
 }];
 }
 else
 {
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self
 cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
 [alert show];
 }
 }
 else
 {
 UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Message" message:@"Some Problem occured while Payment please , try again .." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
 [alert show];
 
 [self.navigationController popViewControllerAnimated:YES];
 
 }
 
 }
 
 - (void)paymentSuccessWithKey:(NSString *)payKey andStatus:(PayPalPaymentStatus)paymentStatus
 {
 [self sendPaymentTransactionID:payKey];
 
 NSString *severity = [[PayPal getPayPalInst].responseMessage objectForKey:@"severity"];
 NSLog(@"severity: %@", severity);
 NSString *category = [[PayPal getPayPalInst].responseMessage objectForKey:@"category"];
 NSLog(@"category: %@", category);
 NSString *errorId = [[PayPal getPayPalInst].responseMessage objectForKey:@"errorId"];
 NSLog(@"errorId: %@", errorId);
 NSString *message = [[PayPal getPayPalInst].responseMessage objectForKey:@"message"];
 NSLog(@"message: %@", message);
 
 status = PAYMENTSTATUS_SUCCESS;
 }
 
 - (void)paymentFailedWithCorrelationID:(NSString *)correlationID
 {
 NSString *severity = [[PayPal getPayPalInst].responseMessage objectForKey:@"severity"];
 NSLog(@"severity: %@", severity);
 NSString *category = [[PayPal getPayPalInst].responseMessage objectForKey:@"category"];
 NSLog(@"category: %@", category);
 NSString *errorId = [[PayPal getPayPalInst].responseMessage objectForKey:@"errorId"];
 NSLog(@"errorId: %@", errorId);
 NSString *message = [[PayPal getPayPalInst].responseMessage objectForKey:@"message"];
 NSLog(@"message: %@", message);
 
 status = PAYMENTSTATUS_FAILED;
 }
 
 
 - (void)paymentCanceled
 {
 status = PAYMENTSTATUS_CANCELED;
 }
 
 - (PayPalAmounts *)adjustAmountsForAddress:(PayPalAddress const *)inAddress andCurrency:(NSString const *)inCurrency andAmount:(NSDecimalNumber const *)inAmount
 andTax:(NSDecimalNumber const *)inTax andShipping:(NSDecimalNumber const *)inShipping andErrorCode:(PayPalAmountErrorCode *)outErrorCode
 {
 //do any logic here that would adjust the amount based on the shipping address
 PayPalAmounts *newAmounts = [[PayPalAmounts alloc] init];
 return newAmounts;
 }
 
 - (NSMutableArray *)adjustAmountsAdvancedForAddress:(PayPalAddress const *)inAddress andCurrency:(NSString const *)inCurrency
 andReceiverAmounts:(NSMutableArray *)receiverAmounts andErrorCode:(PayPalAmountErrorCode *)outErrorCode
 {
 NSMutableArray *returnArray = [NSMutableArray arrayWithCapacity:[receiverAmounts count]];
 
 return returnArray;
 }
 
 - (void)paymentLibraryExit
 {
 UIAlertView *alert = nil;
 switch (status)
 {
 case PAYMENTSTATUS_SUCCESS:
 NSLog(@"Payment by PayPal - Success");
 //[[AppDelegate sharedAppDelegate]showToastMessage:NSLocalizedString(@"PROCESSING_PAYMENT", nil)];
 break;
 case PAYMENTSTATUS_FAILED:
 alert = [[UIAlertView alloc] initWithTitle:nil
 message:@"Payment has been failed! Admin or Driver's Paypal credentials maybe incorrect."
 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 break;
 case PAYMENTSTATUS_CANCELED:
 alert = [[UIAlertView alloc] initWithTitle:nil
 message:@"You have cancelled your payment."
 delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
 break;
 }
 [alert show];
 }*/

#pragma mark -
#pragma mark - UITextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished)
     {
     }];
    
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)confirmBtnPressed:(id)sender
{
    [self.btnFeedBack setTitle:@"  Feedback" forState:UIControlStateNormal];
    self.viewForBill.hidden=YES;
    self.navigationController.navigationBarHidden=NO;
    
    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(150, 27) AndPosition:CGPointMake(85, 190)];
    ratingView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:ratingView];
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                    toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=NSLocalizedString(@"COMMENT", nil);
    }
    
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
