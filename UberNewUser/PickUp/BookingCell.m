//
//  BookingCell.m
//  TaxiNow
//
//  Created by Sapana Ranipa on 05/11/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BookingCell.h"

@implementation BookingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
