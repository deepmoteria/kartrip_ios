//
//  ReferralCodeVC.m
//  UberforXOwner
//
//  Created by Deep Gami on 21/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ReferralCodeVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"


@interface ReferralCodeVC ()
{
    NSString *strForReferralCode;
}

@end

@implementation ReferralCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self SetLocalization];
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getReferralCode];
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btnNavigation setTitle:NSLocalizedString(@"Referral Code", nil) forState:UIControlStateNormal];
}
-(void)SetLocalization
{
    self.lblYour.text=NSLocalizedString(@"Your Referral Code is", nil);
    self.lblCredit.text=NSLocalizedString(@"Your Referral Credit", nil);
    [self.btnShare setTitle:NSLocalizedString(@"Share With your friend", nil) forState:UIControlStateNormal];
    [self.btnShare setTitle:NSLocalizedString(@"Share With your friend", nil) forState:UIControlStateHighlighted];
    [self.btnShare setTitle:NSLocalizedString(@"Share With your friend", nil) forState:UIControlStateSelected];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getReferralCode
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSString *strForUserId=[PREF objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[PREF objectForKey:PREF_USER_TOKEN];
        
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_REFERRAL,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     strForReferralCode=[response valueForKey:@"referral_code"];
                     [PREF setObject:strForReferralCode forKey:PREF_REFERRAL_CODE];
                     [PREF synchronize];
                     self.lblCode.text=strForReferralCode;
                     self.lblBalance.text=[NSString stringWithFormat:@"$ %@",[response valueForKey:@"balance_amount"]];
                 }
                 else
                 {
                     NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                     if([str isEqualToString:@"11"])
                     {
                         [self performSegueWithIdentifier:SEGUE_UNWIND sender:nil];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}

- (IBAction)shareBtnPressed:(id)sender
{
    NSString *texttoshare = [NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"My Referral code is", Nil),strForReferralCode]; //this is your text string to share
    //UIImage *imagetoshare = @""; //this is your image to share
    NSArray *activityItems = @[texttoshare];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
    [self presentViewController:activityVC animated:TRUE completion:nil];
    
}
-(void)shareMail
{
    if(strForReferralCode)
    {
        
        if ([MFMailComposeViewController canSendMail])
        {
            MFMailComposeViewController *mailer=[[MFMailComposeViewController alloc]init ];
            mailer.mailComposeDelegate=self;
            NSArray *toRecipients=[[NSArray alloc]initWithObjects:@"",nil];
            NSString *msg=[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"My Referral code is", Nil),strForReferralCode];
            [mailer setSubject:@"SHARE REFERRAL CODE"];
            [mailer setMessageBody:msg isHTML:NO];
            
            [mailer setDefinesPresentationContext:YES];
            [mailer setEditing:YES];
            [mailer setModalInPopover:YES];
            [mailer setNavigationBarHidden:NO animated:YES];
            [mailer setWantsFullScreenLayout:YES];
            
            [self presentViewController:mailer animated:YES completion:nil];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                            message:@"Your device doesn't support the composer sheet"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles: nil];
            [alert show];
            
        }
    }
    else
        
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_REFERRAL", nil)];
        
    }
    
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail send canceled");
            [self.tabBarController setSelectedIndex:0];
            [self dismissViewControllerAnimated:YES completion:NULL];
            break;
        case MFMailComposeResultSent:
            [self showAlert:@"Mail sent successfully." message:@"Success"];
            [self dismissViewControllerAnimated:YES completion:NULL];
            
            NSLog(@"Mail send successfully");
            break;
        case MFMailComposeResultSaved:
            [self showAlert:@"Mail saved to drafts successfully." message:@"Mail saved"];
            NSLog(@"Mail Saved");
            break;
        case MFMailComposeResultFailed:
            [self showAlert:[NSString stringWithFormat:@"Error:%@.", [error localizedDescription]] message:@"Failed to send mail"];
            NSLog(@"Mail send error : %@",[error localizedDescription]);
            break;
        default:
            break;
    }
}

- (void)showAlert:(NSString *)title message:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}


- (IBAction)BackButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
